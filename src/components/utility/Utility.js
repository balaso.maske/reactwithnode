export function InputValidation(inputObject, inputValue){
    let isValid = true;
    if(inputObject.required){
        if(inputValue === ""){
            isValid = "<b>" + inputObject.label + "</b> is required";
            if(isValid !== true){
                return isValid;
            }
        }
        if(inputObject.inputType === "email"){
            isValid = emailValidation(inputObject, inputValue);
            if(isValid !== true){
                return isValid;
            }
        }
        
        if(inputObject.minLength){
            isValid = checkMinimumLength(inputObject, inputValue);
            if(isValid !== true){
                return isValid;
            }
        }
        if(inputObject.maxLength){
            isValid = checkMaximumLength(inputObject, inputValue);
            if(isValid !== true){
                return isValid;
            }
        }
        if(inputObject.inputType === "uiMultiSelect"){
            if(inputValue.length === 0){
                return "<b>" + inputObject.label + "</b> is required";
            }else{
                return true;
            } 
        }
        return isValid;
    }else{
        if(inputObject.inputType === "email" && inputValue !== ""){
            return emailValidation(inputObject, inputValue);
        }
        return true;
    }
}

export function emailValidation(inputObject, inputValue){
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(inputValue)) {
        return  "<b>" + inputValue + "</b> is not a valid email address";
    }else{
        return true;
    }
}

export function checkMinimumLength(inputObject, inputValue){
    if(inputObject.minLength){
        if(inputValue.length < inputObject.minLength){
            return  "<b>" + inputValue + "</b> is shorter than the minimum allowed length " + inputObject.minLength;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

export function checkMaximumLength(inputObject, inputValue){
    if(inputObject.maxLength){
        if(inputValue.length > inputObject.maxLength){
            return  "<b>" + inputValue + "</b> is higher than the maximum allowed length " + inputObject.maxLength;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

export function uimultiSelectRemoveAddedData(inputArray){
    let actualData = [];
    inputArray.forEach((val) => {
        actualData.push(val.data);
    }); 
    return actualData;
}