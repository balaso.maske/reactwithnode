import React, { Component } from 'react';

import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import ReactHtmlParser from 'react-html-parser';

const animatedComponents = makeAnimated();

class UISelect extends Component {

constructor(props){
  super(props);
  this.state = { updatedOptions : [], selectedValue : []};
  }
  componentDidMount(){
    this.getData();
  }
 
  getData(){
    let responseNew = this.props.getDataFunction();
    const { optionValue, optionLabel } = this.props;
    responseNew.then(value => {
      let data = [];
      if(value.success === true){
        data = value.data[0].data;
        data.forEach((option) => {
          option["value"] = option[optionValue];
          option["label"] = option[optionLabel];
        });
      }
      this.setState({ updatedOptions : data });
    }, reason => {
      console.error(reason); // Error!
    });
  }

  setSelectedValue(selectedOption, optionValue){
    let selected = [];
    this.state.updatedOptions.forEach((option) => {
      selectedOption.forEach((val) => {
          if(option[optionValue] === val){
            selected.push(option);
          }
      });
    });
    this.setState({ "selectedValue" : selected });
  }
    
render() {
  const { label, name, required, placeholder, value, errorMsg, isError } = this.props;
  return (
    <div className="form-group">
    {
      label !== "" && 
      <label htmlFor={name} className={required ? 'required' : ''}> {label}</label>
    }
    <Select onChange={this.props.handleChange} name={name}
      placeholder={placeholder}
      closeMenuOnSelect={false}
      components={animatedComponents}
      value={value}
      isMulti
      options={this.state.updatedOptions}
    >
        </Select>
        { isError &&
              <div className="invalid-feedback">
                  {ReactHtmlParser(errorMsg)}
              </div>
       }
    </div>
  );
}
}

export default UISelect;