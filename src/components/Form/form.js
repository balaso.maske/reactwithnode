import React, {Component} from "react";
import TextInput from "./textInput";
import { InputValidation } from "../utility/Utility";
import UISelect from './UISelect';
import CheckboxInput from './Checkbox';
class CustomForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            "errorInput" : "",
            "errorMsg": ""
        }
        this.baseState = this.state;
    }
    onValueChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

          this.setState({
            [name]: value
          });
          
    };
    
    clear(){
        const { formData } = this.props;
        let object = {};
        formData.forEach((input) => {
           object[input.name] = input.value;
        });
        this.setState(object);
    }

    saveData(){
        const { formData } = this.props;
        let isValid = true;
        let inputName = "";
        let errorMessage = "";
       for (let i = 0; i < formData.length; i++) {
           let input = formData[i];
           isValid = InputValidation(input, this.state[input.name]);
           if(isValid !== true){
                inputName = input.name;
                errorMessage = isValid;
                isValid = false;
                break;
           }
        }
        if(isValid){
            this.setState({ errorInput : "", errorMsg : "" })
           this.props.onSave(this.state);
        }else{
            this.setState({ errorInput : inputName, errorMsg : errorMessage })
        }
        console.log(errorMessage)
    }

    componentDidMount(){
        const { formData } = this.props;
        let object = {};
        formData.forEach((input) => {
            let name = input["name"];
            if(input.inputType === "password"){
                object[name] = "";
            }else{
                object[name] = input.value;
            }
        });
        this.setState(object);
        console.log(object);
    }

    handleSelectChange = ( selectedOption, action ) => {
        this.setState({ [action.name] : selectedOption });
    }

    getFormElement(input, index, isEdit){
        const props = {
            name: input.name,
            label: input.label,
            placeholder : input.placeholder,
            value: this.state[input.value],
        }
        if (input.inputType === "uiMultiSelect") {
            return (
            <UISelect handleChange={this.handleSelectChange} key={index} 
                optionValue={input.optionValue} optionLabel={input.optionLabel}
                getDataFunction={input.callback} {...props} />
                );
          }else if(input.inputType === "checkbox"){
              return (
                <CheckboxInput label={input.label} 
                isError={ input.name === this.state.errorInput ? true : false } onValueChange={this.onValueChange} key={input.name} isEdit={isEdit} isEditModeDisplay={input.isEditModeDisplay}
                isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={this.state.errorMsg} />
              )
          }else {
              return (
                <TextInput label={input.label} 
                isError={ input.name === this.state.errorInput ? true : false } 
                onValueChange={this.onValueChange} key={input.name} 
                isEdit={isEdit} isEditModeDisplay={input.isEditModeDisplay}
                isDisabled={input.isDisabled} 
                name={input.name} value={this.state[input.name]}
                 inputType={input.inputType} required={input.required}
                 errorMsg={this.state.errorMsg} />
              )
          }
    }

    render() {
       
        const { formData, isEdit } = this.props;
        return (
            <form>
            { formData.map((input, index) => {
                   if (input.inputType === "uiMultiSelect") {
                    return (
                    <UISelect handleChange={this.handleSelectChange} key={index} 
                        name={input.name} label={input.label}
                        isError = {input.name === this.state.errorInput ? true : false }
                        placeholder={input.placeholder}
                        value={this.state[input.name]}
                        data={input.data}
                        optionValue={input.optionValue} optionLabel={input.optionLabel}
                        getDataFunction={input.callback}
                        required={input.required}
                        errorMsg={this.state.errorMsg} />
                        );
                  }else if(input.inputType === "checkbox"){
                      return (
                        <CheckboxInput label={input.label} isError={ input.name === this.state.errorInput ? true : false }onValueChange={this.onValueChange} key={input.name} isEdit={isEdit} isEditModeDisplay={input.isEditModeDisplay}
                        isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={input.errorMsg} />
                      )
                  }else {
                      return (
                        <TextInput label={input.label} 
                        isError={ input.name === this.state.errorInput ? true : false }
                        onValueChange={this.onValueChange}
                         key={input.name} 
                         isEdit={isEdit} isEditModeDisplay={input.isEditModeDisplay}
                        isDisabled={input.isDisabled} name={input.name} 
                        value={this.state[input.name]} inputType={input.inputType}
                        required={input.required}
                        errorMsg={this.state.errorMsg} />
                      )
                  }
                }
            )}
            <center>
                    
                       <button type="button" className="btn btn-success" onClick={(e)=>{this.saveData()}}> { isEdit ? 'Update' : 'Save'} </button> &nbsp; 
                       { isEdit === false &&
                        <button type="button" className="btn btn-warning" onClick={()=>{ this.clear()}}>Reset</button>
                        }
                    </center>
           </form>
        );
    }
}

export default CustomForm