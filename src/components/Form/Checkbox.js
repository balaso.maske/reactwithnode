
import React from "react";

function CheckboxInput (props) {
   
        const { name, value, required, label, isEditModeDisplay, isDisabled, isError, errorMsg } = props;
        return (
            <>
            {
                ( isEditModeDisplay === false || isEditModeDisplay === undefined) &&
                <div className="form-check">
                  <input type="checkbox" className="form-check-input" name={name} onChange={props.onValueChange} checked={value} disabled={isDisabled} />
                  {
                    label !== "" && 
                    <label htmlFor={name} className={required ? 'required' : ''}> {label} </label>
                  }
                { isError &&
                <div className="invalid-feedback">
                {errorMsg}
              </div>
                }
             </div>
            }
          </>
        );
    }

export default CheckboxInput