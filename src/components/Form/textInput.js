import React from "react";
import ReactHtmlParser from 'react-html-parser';


function TextInput (props) {
   
        const { name, value, required, inputType, label, isEditModeDisplay, isDisabled, isError, errorMsg } = props;
        
        return (
            <>
            {
                ( isEditModeDisplay === false || isEditModeDisplay === undefined) &&
                <div className="form-group">
                  {
                    label !== "" && 
                    <label htmlFor={name} className={required ? 'required' : ''}> {label} </label>
                  }
                <input type={inputType} value={value} 
                   onChange={props.onValueChange}
                   className={ isError ? 'form-control is-invalid' : 'form-control' }  placeholder={label} name={name} disabled={isDisabled} required/>
                { isError &&
                <div className="invalid-feedback">
                {ReactHtmlParser(errorMsg)}
              </div>
                }
             </div>
            }
          </>
        );
    }

export default TextInput