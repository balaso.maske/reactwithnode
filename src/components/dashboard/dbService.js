import database from '../../../src/database/database.json';


export function getFilterInfo(name, searchKey){
    if(database[name] !== undefined){
        let filters = [];
        /*database[name].forEach((data) => {
            //filters.push({ "name": data["name"], "isSelected": false});
        });*/
        database.data.forEach((row) => {
            filters.push({ "name": row[name], "isSelected": false});
        });
        return Array.from(new Set(filters));
    }else{
        return [];
    }
}