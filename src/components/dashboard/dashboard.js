
import React, { Component, useState } from 'react';
import { connect } from "react-redux";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import * as dbService from "./dbService";

import { Icon } from '../FontAwesome';
import './dashboard.css';
class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false,
            selectedFilterDimension: {},
            attributes: [],
            selectedFilters: [],
            selectedFiltersCount: 0,
            searchFilterText: '',
            sortColumnName: '',
            sortOrder: 'asc',
            rowPerPage: 5,
            currentPage: 1,
            result: {
                "headers": [],
                "dataTypes": [],
                "values": [],
                "displayResult": [],
                "totalResult": 0
            },
            selectedDimensionFilter: [],
            availableFilters: {},
            config: {
                "dimensions": [
                    {
                        "name": "Company",
                        "column": "company",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 0,
                        "displayName": "Company",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": true
                    },
                    {
                        "name": "Description",
                        "column": "description",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 3,
                        "displayName": "Company Description",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": false
                    },
                    {
                        "name": "posted_on",
                        "column": "posted_on",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 2,
                        "displayName": "Posted On",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": true
                    },
                    {
                        "name": "Skills",
                        "column": "skills",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 4,
                        "displayName": "Skills",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": true
                    },
                    {
                        "name": "Country",
                        "column": "country",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 7,
                        "displayName": "Country",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": true
                    },
                    {
                        "name": "Name",
                        "column": "name",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 5,
                        "displayName": "Name",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": false
                    },
                    {
                        "name": "Email",
                        "column": "email",
                        "filterColumn": null,
                        "join": null,
                        "tableName": "",
                        "aliasName": "",
                        "query": " ",
                        "displayIndex": 6,
                        "displayName": "Email",
                        "displayType": "mainDimension",
                        "isSelected": false,
                        "isFilterSupport": false
                    }
                ]
            }
        };
    }
    handle(state) {
        this.setState({ ...this.state, isModalOpen: state });
    }
    removeAllSelectedFilter(filter) {
        let tempAvailableFilter = this.state.availableFilters;
        if (tempAvailableFilter[filter.name] === undefined) {
            return null;
        }
        let tempSelectedFilter = [];
        this.state.selectedFilters.forEach((selectedFilter, index) => {
            if (selectedFilter.name === filter.name) {
                selectedFilter["values"] = [];
            }
            tempSelectedFilter.push(selectedFilter);
        });

        tempAvailableFilter[filter.name].forEach((filter, index) => {
            filter.isSelected = false;
        });

        this.setState({ ...this.state, selectedFilters: tempSelectedFilter });
    }

    clickFilterIcon(dimension) {
        let filters = this.getFilterInfo(dimension);
        this.setState({ ...this.state, selectedFilterDimension: dimension, isModalOpen: true, selectedDimensionFilter: filters, searchFilterText: '' });
    }
    getFilterInfo(dimension) {
        if (this.state.availableFilters[dimension.name] !== undefined) {
            return this.state.availableFilters[dimension.name];
        } else {
            let filters = dbService.getFilterInfo(dimension.name, "");
            this.state.availableFilters[dimension.name] = filters;
            let newFilters = this.state.availableFilters;
            newFilters[dimension.name] = filters;
            this.setState({ ...this.state, availableFilters: newFilters });
            return filters;
            // TODO : get Filter from service
        }
    }
    getSelectedFilters(dimension) {
        let selectedFilters = [];
        this.state.selectedFilters.forEach((filter) => {
            if (dimension.name === filter.name) {
                selectedFilters = filter.values;
            }
        });

        return selectedFilters;
    }
    toggleDimension(dimension) {
        let tempDimensions = [];
        this.state.config.dimensions.forEach((dimensions) => {
            if (dimensions.name === dimension.name) {
                dimensions.isSelected = !dimensions.isSelected;
            }
            tempDimensions.push(dimensions);
        });
        let tempAttribute = this.state.attributes;
        if (dimension.isSelected === false) {
            let tempAttribute2 = [];
            tempAttribute.forEach((attribute, index) => {
                if (attribute.name !== dimension.name) {
                    tempAttribute2.push(attribute);
                }
            });
            tempAttribute = tempAttribute2;
        } else {
            tempAttribute.push(dimension);
        }
        this.setState({ ...this.state, isModalOpen: false, attributes: tempAttribute, config: { "dimensions": tempDimensions } }, () => {
        });
    }

    toggleFilter(selectedFilterDimension, filter, isSelected) {
        let found = false;
        let filterCount = 0;
        let existingFilters = this.state.selectedFilters;
        this.state.selectedFilters.forEach((selectedFilter, index) => {
            if (selectedFilter.name === selectedFilterDimension.displayName) {
                found = true;
            } else {
                found = false;
            }
        });

        if (found == false) {
            let tempFilterObject = {};
            tempFilterObject["displayName"] = selectedFilterDimension.displayName;
            tempFilterObject["name"] = selectedFilterDimension.name;
            tempFilterObject["values"] = [];
            tempFilterObject["values"].push(filter);
            filterCount = tempFilterObject["values"].length;
            existingFilters.push(tempFilterObject);
        } else {
            existingFilters.forEach((selectedFilter, index) => {
                if (selectedFilter.name === selectedFilterDimension.displayName) {
                    found = true;
                    if (isSelected) {
                        let tempSelectedFilter = [];
                        selectedFilter.values.forEach((value, index) => {
                            if (value !== filter) {
                                tempSelectedFilter.push(value);
                            }
                        });
                        selectedFilter["values"] = tempSelectedFilter;
                    } else {
                        selectedFilter["values"].push(filter);
                    }
                    filterCount = selectedFilter["values"].length;
                } else {
                    found = false;
                }
            });
        }

        /*** existing dimension filter set isSelect***/
        let tempFilter = this.state.availableFilters;
        let availableDimensionsFilter = tempFilter[selectedFilterDimension.name];
        availableDimensionsFilter.forEach((availableFilter, index) => {
            if (availableFilter.name === filter) {
                availableFilter.isSelected = !availableFilter.isSelected
            }
        });
        tempFilter[selectedFilterDimension.name] = availableDimensionsFilter;
        this.setState({ ...this.state, selectedFilters: existingFilters, availableFilters: tempFilter });
    }

    openFilterModal(filter) {
        this.state.config.dimensions.forEach((dimension) => {
            if (dimension.name === filter) {
                return this.clickFilterIcon(dimension)
            }
        });
    }
    renderFilterView(filter, index) {
        if (filter.values.length > 0) {
            if (filter.values.length > 3) {
                return (
                    <span type="button" className="btn btn-default buttonFilterSpan" key={index} title={filter.displayName + " : " + filter.values.toString()} onClick={() => this.openFilterModal(filter.name)}>
                        {filter.displayName}:({filter.values.length})
                        <span className="pull-right" onClick={() => this.removeAllSelectedFilter(filter)}>
                            &nbsp;  <i>
                                <Icon iconName={"FaTimesCircle"} />
                            </i>
                        </span>
                    </span>
                )
            } else {
                return (
                    <span type="button" className="btn btn-default buttonFilterSpan" key={index} onClick={() => this.openFilterModal(filter.name)}>
                        {filter.displayName} &nbsp; : &nbsp;
                        {filter.values.map((value, valIndex) => (
                            <span key={valIndex}>{value}
                                { valIndex < filter.values.length - 1 ? " , " : ""}
                            </span>
                        ))}
                        <span className="pull-right" onClick={() => this.removeAllSelectedFilter(filter)}>
                            &nbsp;  <i>
                                <Icon iconName={"FaTimesCircle"} />
                            </i>
                        </span>
                    </span>
                )
            }
        } else {
            return (<></>)
        }

    }

    onSortChange(val, index, sortOrder) {
        let order = "asc";

        if(sortOrder !== undefined) {
            order = sortOrder;
        }else{
            order = this.state.sortOrder === 'asc' ? "desc" : "asc";
        }
        
        let tempResult = this.state.result;
        if (this.state.result.values.length > 1000) {
            // get Data from Service
        } else {
            tempResult.values.sort(function (data1, data2) {
                var data11 = data1[index], data21 = data2[index];
                if (data11 == data21) return 0;
                if (order == 'desc') {
                    return data11 < data21 ? 1 : -1;
                } else {
                    return data11 > data21 ? 1 : -1;

                }
            });
        }
        this.setState({  ...this.state, sortColumnName: val, sortOrder: order, result: tempResult, currentPage : 1 }, () => {
            this.spliceResult();
        });
    }
    onValueChange(val) {
        this.setState({ ...this.state, searchFilterText: val });
    }

    generateSelectedFilterDOM() {
        let filters = this.getSelectedFilters(this.state.selectedFilterDimension);
        if (filters.length > 0) {
            return (
                <div className="well well-sm">
                    <strong>
                        Selected {this.state.selectedFilterDimension.displayName} ({filters.length}) : &nbsp;
                </strong>
                    <span className="">
                        {filters.map((filter, index) => (
                            <span type="button" className="btn btn-default buttonSpan" key={index} >
                                {filter} &nbsp;
                                <span className="pull-right" onClick={() => this.toggleFilter(this.state.selectedFilterDimension, filter, true)}>
                                    <i>
                                        <Icon iconName={"FaTimesCircle"} />
                                    </i>
                                </span>
                            </span>
                        ))}
                    </span>
                </div>
            )
        } else {
            return (<></>)
        }

    }

    getData() {
        let resultObject = {
            "headers": [],
            "dataTypes": [],
            "values": [],
            "displayResult": [],
            "totalResult": 500,
            "attributes": []
        };

        resultObject.attributes = this.state.attributes;

        this.state.attributes.forEach((attribute) => {
            resultObject.headers.push(attribute.displayName);
            resultObject.dataTypes.push("String");
        });

        for (let i = 0; i < resultObject.totalResult; i++) {
            let resultValue = [];
            for (let j = 0; j < resultObject.dataTypes.length; j++) {
              //  resultValue.push(resultObject.attributes[j].name + i);
              resultValue.push(( i + 1));
            }

            resultObject.values.push(resultValue);
        }
        this.setState({ ...this.state, result: resultObject }, () => {
            this.spliceResult();
        });
    }

    spliceResult() {
        let tempResult = this.state.result;
        let fromPosition = (this.state.rowPerPage * (this.state.currentPage - 1));
        let toPosition = (this.state.rowPerPage * this.state.currentPage) - 1;
        let spliceResultValues = [];
        tempResult.values.forEach((value, index) => {
            if (index >= fromPosition && index <= toPosition) {
                spliceResultValues.push(value);
            }
        });
        tempResult["displayResult"] = spliceResultValues;
        this.setState({ ...this.state, result: tempResult }, () => {
        });

    }

    generateTableHeader(value, index) {
        let attribute = this.state.result.attributes[index];
        return (
            <th  className={"clickableElement " + (attribute.name === this.state.sortColumnName.name ? " activeTableHeader" : "")} key={index}>
                <div  style={{ float: "", width: 85 + "%"}}>
                <span onClick={() => this.onSortChange(attribute, index, undefined)}>
                <span className={"clickableElement columnLeftText "} title={value}> {value} &nbsp;</span>
                {
                    attribute.name !== this.state.sortColumnName.name &&
                    <Icon iconName="FaSort" />
                }
                {
                    (attribute.name === this.state.sortColumnName.name && this.state.sortOrder === "asc") &&
                    <Icon iconName="FaSortAlphaUp" />
                }
                {
                    (attribute.name === this.state.sortColumnName.name && this.state.sortOrder === "desc") &&
                    <Icon iconName="FaSortAlphaDown" />
                }
               </span>
                <Dropdown as={ButtonGroup} className="table-header-dropdown">
                    <Dropdown.Toggle split id="dropdown-custom" className="dropdown-custom" variant="default"> <Icon iconName="FaBars" /></Dropdown.Toggle>
                    <Dropdown.Menu className="super-colors" as="ul">
                        <Dropdown.Item eventKey="1" as="li"
                        onClick={() => this.onSortChange(attribute, index, "asc")} 
                        disabled={attribute.name === this.state.sortColumnName.name && this.state.sortOrder === "asc"}>
                            Sort by Asc
                        </Dropdown.Item>
                        <Dropdown.Item eventKey="2"  as="li"
                        onClick={() => this.onSortChange(attribute, index, "desc")}
                        disabled={attribute.name === this.state.sortColumnName.name && this.state.sortOrder === "desc"}>
                            Sort by Desc
                        </Dropdown.Item>
                        <Dropdown.Item eventKey="3" onClick={() => console.log("On Click Filter")}  as="li">
                             Filter 
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                </div>
            </th>
        )
    }
    handleClick(e) {
        if (e.type === 'click') {
            console.log('Left click');
        } else if (e.type === 'contextmenu') {
            console.log('Right click');
        }
    }
    rowPerPageChange(e) {

        this.setState({ ...this.state, rowPerPage: e.target.value, currentPage: 1 }, () => {
            this.spliceResult();
        });
        // TODO:
    }

    pagination(state) {
        let totalPages = this.state.result.values.length / this.state.rowPerPage;
        let newPageNumber = 1;

        if (state === "next") {
            newPageNumber = this.state.currentPage + 1;
        } else if (state === "last") {
            newPageNumber = totalPages;
        } else if (state === "first") {
            newPageNumber = 1;
        } else if (state === "prev") {
            newPageNumber = this.state.currentPage - 1;
        }

        if (newPageNumber === 0) {
            newPageNumber = 1;
        } else if (newPageNumber > totalPages) {
            newPageNumber = totalPages;
        }
        this.setState({ ...this.state, currentPage: newPageNumber }, () => {
            this.spliceResult();
        });
    }

    generatePaginationDOM() {
        let totalPages = this.state.result.values.length / this.state.rowPerPage;
        return (
            <ButtonGroup aria-label="Pagination" size="md" className="paginationButtonGroup">
                <Button variant="default" className="paginationButton" onClick={() => this.pagination("first")} disabled={this.state.currentPage === 1}>
                    <Icon iconName={"FaStepBackward"} />
                </Button>
                <Button variant="default" className="paginationButton" onClick={() => this.pagination("prev")} disabled={this.state.currentPage === 1}>
                    <Icon iconName={"FaBackward"} />
                </Button>
                <Button variant="default" className="paginationButton" disabled={true}>
                    {(this.state.rowPerPage * (this.state.currentPage - 1)) + 1} - {(this.state.rowPerPage * this.state.currentPage)} of {this.state.result.totalResult}
                </Button>
                <Button variant="default" className="paginationButton" onClick={() => this.pagination("next")} disabled={this.state.currentPage === totalPages}>
                    <Icon iconName={"FaForward"} />
                </Button>
                <Button variant="default" className="paginationButton" onClick={() => this.pagination("last")} disabled={this.state.currentPage === totalPages}>
                    <Icon iconName={"FaStepForward"} />
                </Button>
            </ButtonGroup>
        )
    }

    searchFilter = () => {
        debugger;
        // TODO : get Filter from DB
    }

    render() {
        const { isAdmin, isAdminView } = this.props;
        return (
            <div>

                <div className="row">
                    <div className="col-md-9 col-lg-9"> <h2> Welcome to Dashboard Page </h2></div>

                    <div className="col-md-3 col-lg-3">
                        <Button variant="success" onClick={() => this.getData()} style={{ float: "right" }}>
                            Run
                        </Button>
                    </div>
                </div>
                <div className="row">

                    <div className="col-sm-3 col-md-3 col-lg-3 nopadding">
                        <div className="row tableRowsWithButtom" >
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
                                <Accordion defaultActiveKey="1">
                                    <Card>
                                        <Card.Header className="panelHeadingCss">
                                            <div style={{ float: "left" }}>
                                                    Select Time Periods
                                                </div>
                                                <div style={{ float: "right" }}>
                                                    <Icon iconName={"FaClock"}></Icon>
                                                </div>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="0" className="show">
                                            <Card.Body className="dashboardPanelBody">
                                                Time Period
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                    <Card>
                                        <Card.Header className="panelHeadingCss">
                                            <div style={{ float: "left" }}>
                                                    Select Dimensions
                                                </div>
                                                <div style={{ float: "right" }}>
                                                    <Icon iconName={"FaCubes"}></Icon>
                                                </div>
                                        </Card.Header>
                                        <Accordion.Collapse eventKey="1" className="show">
                                            <Card.Body className="dashboardPanelBody">
                                                <ListGroup as="ul">
                                                    {this.state.config.dimensions.sort((a, b) => a.displayIndex > b.displayIndex ? 1 : -1).map((dimension, index) => (
                                                        <ListGroup.Item as="li" key={index} className="filterIcon">
                                                            {dimension.displayName}
                                                            <span className="pull-right">
                                                                {
                                                                    dimension.isFilterSupport &&
                                                                    <i style={{ float: 'left' }} className="filterIconDisplay" onClick={() => this.clickFilterIcon(dimension)} >
                                                                        <Icon iconName={"FaFilter"} /> &nbsp;
                                                                </i>
                                                                }
                                                                <i style={{ float: 'right' }} onClick={() => this.toggleDimension(dimension)}>
                                                                    <i style={{ display: dimension.isSelected ? 'none' : 'block' }} title={"Click Here to add " + dimension.displayName + " as Dimension"}>
                                                                        <Icon iconName={"FaPlusCircle"} />
                                                                    </i>
                                                                    <i style={{ display: dimension.isSelected ? 'block' : 'none' }} title={"Click Here to remove " + dimension.displayName + " from Dimension"}>
                                                                        <Icon iconName={"FaMinusCircle"} />
                                                                    </i>
                                                                </i>
                                                            </span>
                                                        </ListGroup.Item>
                                                    ))}
                                                </ListGroup>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-9 col-md-9 col-lg-9 pull-right">
                        <div className="row" style={{ paddingLeft: 10 + 'px' }}>
                            <div className="col-lg-12 col-md-12 row tableRowsWithoutButtom">
                                <div className="col-sm-5 col-md-3 col-lg-2 text-center" style={{ padding: 5 + 'px' }}>
                                    <span className="nobottomMargin pull-left" >Filters</span>
                                    <h5>
                                        <i style={{ float: 'right' }} >
                                            <Icon iconName={"FaFilter"} /> &nbsp;
                                        </i>
                                        <i className="fa fa-filter fa-lg pull-right" style={{ marginTop: -6 + 'px', marginRight: 2 + 'px' }}></i></h5>
                                </div>
                                <div className="col-sm-7 col-md-9 col-lg-10 nopadding onlyleftborder minHeight" style={{ padding: 1 + 'px' }}>
                                    <div className="well well-sm nomargin" style={{ padding: 0 + 'px' }}>
                                        {this.state.selectedFilters.map((filter, index) => (
                                            this.renderFilterView(filter, index)
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-md-12 row tableRowsWithoutButtom  borderBottom">
                                <div className="col-sm-5 col-md-3 col-lg-2 text-center" style={{ padding: 5 + 'px' }}>
                                    <span className="nobottomMargin pull-left" style={{ marginTop: 3 + 'px', marginLeft: 0 + 'px' }}>
                                        KPIs/Dimensions </span>
                                    <h5>

                                        <i style={{ float: 'right' }} >
                                            <Icon iconName={"FaCube"} />
                                        </i></h5>
                                </div>
                                <div className="col-sm-7 col-md-9 col-lg-10 nopadding onlyleftborder dndArea minHeight" style={{ padding: 1 + 'px' }}>
                                    <div className="well well-sm nomargin" style={{ padding: 0 + 'px' }}>
                                        {this.state.attributes.map((dimension, index) => (
                                            <span type="button" className="btn btn-default buttonSpan" key={index} >
                                                {dimension.displayName} &nbsp;
                                                <span className="pull-right" onClick={() => this.toggleDimension(dimension)}>
                                                    <i style={{ display: dimension.isSelected ? 'block' : 'none' }}>
                                                        <Icon iconName={"FaTimesCircle"} />
                                                    </i>
                                                </span>
                                            </span>
                                        ))}
                                    </div>
                                </div>

                            </div>
                            <div className="row" style={{ width: 100 + "%" }}>
                                <div className="col-sm-12 col-md-12 col-lg-12 outerBox">
                                    <div className="innerBox">
                                        <div className="col-sm-12 col-md-12 col-lg-12" style={{ marginBottom: 5 + "%", padding: 0 + 'px' }}>
                                            <div className="pull-left" >

                                            </div>
                                            <div style={{ float: "right" }}>
                                                <Button variant="primary" disabled={this.state.result.values.length === 0} onClick={() => this.handle(false)} title="Export data as Excel">
                                                    <Icon iconName={"FaDownload"} /> Export
                                        </Button>
                                            </div>
                                        </div>
                                        <div className="table-responsive" style={{display: this.state.result.values.length > 0 ? "block":"none" }}>
                                            <table className="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        {this.state.result.headers.map((value, index) => (
                                                            this.generateTableHeader(value, index)
                                                        ))}
                                                    </tr>
                                                </thead>
                                                <tbody className={this.state.result.displayResult.length > 5 ? "scrollerTable" : ""}>
                                                    {this.state.result.displayResult.map((value, index) => (
                                                        <tr key={index} onClick={this.handleClick} onContextMenu={this.handleClick}>
                                                            {value.map((val, tdIndex) => (
                                                                <td key={tdIndex}>{val}</td>
                                                            ))}
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                        {
                                            this.state.result.values.length > 0 &&
                                            <div className="col-sm-12 col-md-12 col-lg-12">
                                                <div style={{ float: "right" }}>
                                                    <Form>
                                                        <Form.Group as={Row}>
                                                            <Form.Control value={this.state.rowPerPage} as="select" size="md" onChange={(event) => this.rowPerPageChange(event)}>
                                                                <option>5</option>
                                                                <option>10</option>
                                                                <option>25</option>
                                                                <option>50</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Form>
                                                </div>
                                                <div>
                                                    {this.generatePaginationDOM()}
                                                </div>
                                            </div>
                                        }

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.isModalOpen} onHide={() => this.handle(false)} backdrop="static"
                    keyboard={false} size="lg">
                    <Modal.Header>
                        <Modal.Title>
                            <Icon iconName={"FaCog"} /> &nbsp; Select {this.state.selectedFilterDimension.displayName}

                        </Modal.Title>
                        <span className="pull-right btn btn-primary btn-sm" data-ng-disabled="getFilterByName(selectedDimension).values.length == 0"
                            onClick={() => this.removeAllSelectedFilter(this.state.selectedFilterDimension)}>
                            <Icon iconName={"FaTimesCircle"} /> Clear All Filters
                        </span>
                    </Modal.Header>
                    <Modal.Body>
                        {this.generateSelectedFilterDOM()}
                        <div className="form-group">
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" name="filterSearch"
                                    onChange={(event) => this.onValueChange(event.target.value)}
                                    placeholder={'Search ' + this.state.selectedFilterDimension.displayName} 
                                    onKeyPress={(event)=> event.key === "Enter" ? this.searchFilter() : null} />
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <Icon iconName={"FaSearch"} />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="">
                            {
                                this.state.selectedDimensionFilter.filter(val => val.name.toLowerCase().includes(this.state.searchFilterText.toLowerCase())).length > 0 &&
                                <span>Displaying <b> {this.state.selectedDimensionFilter.filter(val => val.name.toLowerCase().includes(this.state.searchFilterText.toLowerCase())).length} </b> records</span>
                            }
                            {
                                this.state.selectedDimensionFilter.filter(val => val.name.toLowerCase().includes(this.state.searchFilterText.toLowerCase())).length === 0 &&
                                <span className="text-info">There are no results for this {this.state.selectedFilterDimension.displayName} search </span>
                            }
                        </div>
                        <ListGroup as="ul" className={this.state.selectedDimensionFilter.length > 4 ? "listgroupScroll" : ""}>
                            {this.state.selectedDimensionFilter.filter(val => val.name.toLowerCase().includes(this.state.searchFilterText.toLowerCase())).sort((a, b) => a.name > b.name ? 1 : -1).sort((a, b) => a.isSelected > b.isSelected ? -1 : 1).map((filter, index) => (
                                <ListGroup.Item as="li" key={index} className="clickable filter-list-group-item" onClick={() => this.toggleFilter(this.state.selectedFilterDimension, filter.name, filter.isSelected)}>
                                    {filter.name}
                                    <span className="pull-right" style={{ float: 'left', display: filter.isSelected ? 'block' : 'none' }}>
                                        <Icon iconName={"FaCheck"} />
                                    </span>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => this.handle(false)}>
                            OK
          </Button>
                    </Modal.Footer>
                </Modal>
            </div >
        )
    }
}

const mapStateToProps = state => ({
    isAdmin: state.commonReducer.isAdmin,
    isAdminView: state.commonReducer.isAdminView
});

export default connect(mapStateToProps)(Dashboard);