import React, {Component} from 'react';
import { UIView } from '@uirouter/react';
import {connect} from "react-redux";
import TopMenubar from './MenuBar/TopMenuBar';
import LeftMenubar from './MenuBar/LeftMenuBar';

class Home extends Component {
    render(){
        
        const { isLeftMenuOpen, isAuthenticated } = this.props;
        const viewHeight = window.outerHeight - 168;
        return(
       <div className="container-fluid">
        { isAuthenticated &&
        <TopMenubar />
        } 
            <div style={{paddingTop: 62 + 'px'}} className="row">
                {  isLeftMenuOpen && isAuthenticated &&
                <div className="col-md-2 sidebar" style={{ height: ( viewHeight), padding : 0 + 'px' }}>
                    <LeftMenubar/>
                </div>
                }
                    <div className={isLeftMenuOpen && isAuthenticated ? "col-md-10" : "col-md-12"} style={{paddingTop: 10 + 'px', maxHeight: ( viewHeight), overflow: 'auto'}}>
                        <UIView/>
                       
                    </div>
            </div>
    </div>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.commonReducer.loading,
    entity : state.entityReducer.selectedEntity,
    isLeftMenuOpen : state.commonReducer.isLeftMenuOpen,
    isAuthenticated : state.commonReducer.isAuthenticated
});

export default connect(mapStateToProps)(Home);