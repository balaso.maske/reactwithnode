import React, {Component} from 'react';
import {UISref} from '@uirouter/react';
import {connect} from "react-redux";
import { Nav, Navbar, NavDropdown} from 'react-bootstrap';
import { FaAngleDoubleLeft, FaAngleDoubleRight} from 'react-icons/fa';
import * as CommonService from "../../services/CommonService";
import { Icon } from '../FontAwesome';
import { RemoveFromStorage } from '../../storage/storage';
//import Logo from "../../../src/assets/image/logo.svg";
//import LogoTechbulls from "../../../src/assets/image/logo.png";
import "./menubar.css"
class TopMenubar extends Component {
    constructor(props){
        super(props);
        this.state = { }
    }
    

    goTo(pageURL){
        let { transition } = this.props;
        transition.router.stateService.go(pageURL);
    }
    leftMenuBar(){
        let s = !this.props.isLeftMenuOpen;
        this.props.dispatch(CommonService.toggleLeftMenu(s));
    }

    showAdminView(status){
        this.props.dispatch(CommonService.showAdminView(status));
    }

    logout(){
      this.props.dispatch(CommonService.isUserAuthenticate({isAuthenticated: false, isAdmin: false, isAdminView : false} ));
      RemoveFromStorage();
     // this.goTo("login");
    }

    setEntity(){
       let object = {
         "name": "User",
         "data": this.props.user
       }
       this.props.dispatch(CommonService.setSelectedEntity(object));
    }
    render(){
        const { isLeftMenuOpen, isAdmin, user } = this.props;

        return(
            <>
            <Navbar bg="dark" variant="dark" fixed="top">
               <Navbar.Brand>
                  <UISref to="welcomepage">
                     <Nav.Link onClick={() => this.showAdminView(false)} style={{color: "#FFFFFF"}}>
                     React Bootstrap
                     </Nav.Link >
                  </UISref>
               </Navbar.Brand>
               <Navbar.Toggle aria-controls="responsive-navbar-nav" />
               <Navbar.Collapse id="responsive-navbar-nav">
                  <Nav className="mr-auto fontWhite">
                     {
                     isLeftMenuOpen && 
                     <FaAngleDoubleLeft onClick={() =>
                     this.leftMenuBar()} />
                     }
                     {
                     isLeftMenuOpen === false && 
                     <FaAngleDoubleRight  onClick={() =>
                     this.leftMenuBar()} />
                     }
                  </Nav>
                  <Nav className="cogsFont">
                     <UISref to="welcomepage">
                        <Nav.Link onClick={() => this.showAdminView(false)}>
                           <Icon iconName="FaHome"/>
                           </Nav.Link >
                     </UISref>
                  </Nav>
                  { isAdmin &&
                  <Nav className="cogsFont">
                     <UISref to="welcomepage">
                        <Nav.Link onClick={() =>
                           this.showAdminView(true)} >
                           <Icon iconName="FaCogs"/>
                        </Nav.Link >
                     </UISref>
                  </Nav>
                  }
                  <Nav>
                     <NavDropdown title={
                     <div className="pull-left">
                        <Icon iconName="FaRegUserCircle"/>
                        &nbsp; 
                        {user.name}
                     </div>
                     } id="collasible-nav-dropdown" className="my-dropdown-toggle">
                     <NavDropdown.Item>
                        <UISref to="userProfile" params={{username : this.props.user.username}} >
                           <span onClick={() => this.setEntity()}> <Icon iconName="FaUser"/> User Profile </span>
                        </UISref>
                     </NavDropdown.Item>
                     <NavDropdown.Item href="#action/3.2">
                        <Icon iconName="FaCog"/>
                        &nbsp; Settings
                     </NavDropdown.Item>
                     
                     <NavDropdown.Divider />
                     <NavDropdown.Item> 
                       <UISref to="login" params={{username : this.props.user.username}} >
                           <span onClick={() => this.logout()}> 
                           <Icon iconName="FaSignOutAlt"/> Log Out </span>
                        </UISref>
                     </NavDropdown.Item>
                     </NavDropdown>
                  </Nav>
               </Navbar.Collapse>
            </Navbar>
            </>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.commonReducer.loading,
    error: state.commonReducer.error,
    entity : state.entityReducer.selectedEntity,
    isLeftMenuOpen : state.commonReducer.isLeftMenuOpen,
    isAdmin : state.commonReducer.isAdmin,
    user : state.commonReducer.user
});

export default connect(mapStateToProps)(TopMenubar);