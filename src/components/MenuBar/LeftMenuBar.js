import React, {Component} from 'react';
import {UISref} from '@uirouter/react';
import {connect} from "react-redux";
import { Nav} from 'react-bootstrap';
import { Icon } from '../FontAwesome';

class LeftMenubar extends Component {

    constructor(props){
        super(props);
        this.state = {
        }
    }
    componentDidMount(){
    }

    userMenu(props) {
        return  (<> 
         <Nav className="menu">
               <UISref to="user">
                  <Nav.Link> <Icon iconName="FaUser"/> User </Nav.Link >
               </UISref>
            </Nav>
            </>
        );
    }
      
    adminMenu(props) {
        return  (<> 
            <Nav className="menu">
               <UISref to="user">
                  <Nav.Link> <Icon iconName="FaUser"/> User </Nav.Link >
               </UISref>
            </Nav>
            <Nav className="menu">
               <UISref to="roles">
                  <Nav.Link> <Icon iconName="FaLowVision"/> Roles </Nav.Link >
               </UISref>
            </Nav>
            <Nav className="menu">
               <UISref to="page">
                  <Nav.Link> <Icon iconName="FaFile"/> Applications </Nav.Link >
               </UISref>
            </Nav>
            <Nav className="menu">
               <UISref to="dashboard">
                  <Nav.Link> <Icon iconName="FaFile"/> Dashboard </Nav.Link >
               </UISref>
            </Nav>
            </>
        );
    }

    render(){
        const { entity, isAdminView, isAdmin } = this.props;
        let entityName  = this.state.entityName;
        if(entity){
            const { name } = entity;
            entityName = name
        }

        return(
            <> 
                {
                    ( isAdmin && isAdminView ) &&
                   this.adminMenu()
                }
                {
                   isAdminView === false &&
                    this.userMenu()
                } 
            </>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.commonReducer.loading,
    error: state.commonReducer.error,
    entity : state.entityReducer.selectedEntity,
    isAdminView : state.commonReducer.isAdminView,
    isAdmin : state.commonReducer.isAdmin,
});

export default connect(mapStateToProps)(LeftMenubar);