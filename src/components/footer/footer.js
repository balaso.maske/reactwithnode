import React, {Component} from "react";

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container footer">
                    <p className="text-muted"> &copy; {new Date().getFullYear()} Copyright: </p>
                    <p> About Me </p>
                </div>
            </footer>
        );
    }
}

export default Footer