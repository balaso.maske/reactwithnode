import React from "react";
import {connect} from "react-redux";
import "../components/user/userprofile.css";
import userImage from "../assets/image/Balaso_Image.jpg"

function AboutMe (props){
        return (
            <div className="container">
<div id="content" className="content p-0">
    <div className="profile-header">
        <div className="profile-header-cover"></div>

        <div className="profile-header-content">
            <div className="profile-header-img">
                <img src={userImage} alt="" />
            </div>

            <div className="profile-header-info">
                <h4 className="m-t-sm"> Balaso B. Maske </h4>
                <p className="m-b-sm"> Full Stack Developer </p>
                <p className="m-b-sm"> 9021459093 / 9595990958 </p>
                <p className="m-b-sm"> balasomaske05@gmail.com </p>
            </div>
        </div>
    </div>

    <div className="profile-container">
        <div className="row row-space-20">
            <div className="col-md-8">
                <div className="tab-content p-0">
                    <div className="tab-pane active show" id="profile-about">
                        <table className="table table-profile">
                            <thead>
                                <tr>
                                    <th colspan="2">WORK AND EDUCATION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="field">Work</td>
                                    <td className="value">
                                        <div className="m-b-5">
                                            <b>Techbulls SoftTech Private Limited </b>
                                            <a href="http://www.techbulls.com/" className="m-l-10" target="_blank">Website</a>
                                            <br />
                                            <span className="text-muted">Senior Software Developer</span>
                                        </div>
                                        <div>
                                            <b>A Java Home </b> 
                                            <a href="http://ajavahome.co.in/" className="m-l-10" target="_blank">Website</a>
                                            <br />
                                            <span className="text-muted">Software Developer</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="field">Education</td>
                                    <td className="value">
                                        <div className="m-b-5">
                                            <b>Master of Computer Application (2015) </b><br />
                                            <span className="text-muted">Savitribai Phule Pune University</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table className="table table-profile">
                            <thead>
                                <tr>
                                    <th colspan="2">About Me</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                    <td className="field">About</td>
                                    <td className="value">
                                    5+ years of experience as Full Stack Developer having knowledge about Java,Spring, MyBatis, MySQL, Amazon RedShift, AngularJS, Bootstrap and AWS. Worked on building scalable backend APIs, writing complex database queries and debugging the same. Also, worked on building dashboards with analytics and charts. 
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td className="field">Linkedin Profile</td>
                                    <td className="value">
                                        <a href="https://www.linkedin.com/in/balaso-maske-420aba102/" target="_blank" className="m-l-10">https://www.linkedin.com/in/balaso-maske-420aba102/</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="field">GitLab</td>
                                    <td className="value">
                                        
                                        <a href="https://gitlab.com/balaso.maske" target="_blank" className="m-l-10">https://gitlab.com/balaso.maske</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table className="table table-profile">
                            <thead>
                                <tr>
                                    <th colspan="2">BASIC INFORMATION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="field">Birth of Date</td>
                                    <td className="value">
                                        30 July 1991
                                    </td>
                                </tr>
                                <tr>
                                    <td className="field">Gender</td>
                                    <td className="value">
                                        Male
                                    </td>
                                </tr>
                                <tr>
                                    <td className="field">Permanent Address</td>
                                    <td className="value">
                                        A/P Ambegaon <br/>
                                        Tal-Kadegaon Dist-Sangli <br/>
                                        Maharashtra Pin- 415305
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div className="col-md-4 hidden-xs hidden-sm">
                <ul className="profile-info-list">
                    <li className="title">PERSONAL INFORMATION</li>
                    <li>
                        <div className="field">Occupation:</div>
                        <div className="value">Senior Software Developer</div>
                    </li>
                    <li>
                        <div className="field">Skills:</div>
                        <div className="value">C++, Node Js, React, HTML5, CSS, jQuery, MYSQL, Bootstrap, Angular JS, Java, Python</div>
                    </li>
                    <li>
                        <div className="field">Country:</div>
                        <div className="value">India (+91)</div>
                    </li>
                    <li>
                        <div className="field">Current Address:</div>
                        <div className="value">
                            <address className="m-b-0">
                                Indira Nagar<br />
                                Vadgaon Sheri, Pune<br />
                                Maharashtra, 411014
                            </address>
                        </div>
                    </li>
                    <li>
                        <div className="field">Phone No.:</div>
                        <div className="value">
                            (+91) 9021459093
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
        )
}


const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(AboutMe);