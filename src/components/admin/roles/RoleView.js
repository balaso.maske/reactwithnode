import React, { Component } from "react";
import {connect} from "react-redux";
import * as RoleService from "../../../services/RoleService";
import Moment from 'react-moment';

class RoleView extends Component{
    constructor(props){
        super(props);
        this.state = { roles :{ pages :[]}, dataAvailable: true };
    }

    componentDidMount(){
        let name = this.props.$stateParams.name;
        let responseNew = this.props.dispatch(RoleService.getRoleByName(name));
        responseNew.then(value => {
            if(value.success === true){
             this.setState({roles : value.data, dataAvailable: true});
            }else{
                this.setState({ roles :{ pages :[]}, dataAvailable: false});
               // ErrorToastr(value.message);
            }
        }, reason => {
            console.error(reason); // Error!
        });
    }

    render(){
        const {name, pages, createdBy, lastModifiedBy, createdDate, lastModifiedDate} = this.state.roles;
        return (
                <div className="container">
                    <div className="well well-lg well-background">
                            <div className="profile-header-content">
                                <h4 className="m-t-sm">{name}</h4>
                            </div>
                        </div>

    <div className="profile-container">
        <div className="row row-space-20">
            <div className="col-md-12 hidden-xs hidden-sm">
                <ul className="profile-info-list">
                    <li className="title">INFORMATION</li>
                    <li>
                        <div className="field">Application:</div>
                        <div className="value">
                            { pages.map((data, index) => (
                                <span key={index} title="">{data.name} { index < ( pages.length - 1) ? "," : ""} </span>
                        ))}
                  </div>
                    </li>
                    </ul>
                    <br/>
                    <table className="table table-profile">
                    <tbody>
                        <tr>
                            <td className="field">Last Modified By:</td>
                            <td className="value">{lastModifiedBy}</td>
                            <td className="field">Last Updated At:</td>
                            <td className="value">
                                <Moment format="MMMM Do YYYY, h:mm:ss a">{lastModifiedDate}</Moment>
                            </td>
                        </tr>
                        <tr>
                            <td className="field">Created By:</td>
                            <td className="value">{createdBy}</td>
                            <td className="field">Created At:</td>
                            <td className="value">
                                <Moment format="MMMM Do YYYY, hh:mm:ss a">{createdDate}</Moment>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                </div>
                </div>
                </div>
            )
    }

}


const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(RoleView);