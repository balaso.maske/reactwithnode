import React, { Component } from "react";
import {connect} from "react-redux";
import * as CommonService from "../../../services/CommonService";
import * as EntityService from "../../../services/EntityService";
import { Icon } from '../../FontAwesome';
import { SuccessToastr } from '../../Toastr';
import  CustomForm from "../../Form/form.js";

class RoleEntry extends Component{
    constructor(props){
        super(props);
        this.state = {
            isEdit : false,
            "pageName": "Role",
            "entityName": "roles",
            error : "",
            pages: [],
            availablePages: []
        }
        this.saveEntity = this.saveEntity.bind(this);
        this.getPages = this.getPages.bind(this);
        
        this.formData = [
            {
                "name": "_id",
                "value": "",
                "required": false,
                "regex": "",
                "label": "ID",
                "inputType": "text",
                "errorMsg": "",
                "isEditModeDisplay": true,
                "isDisabled": true
            },
            {
              "name": "name",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Name",
              "inputType": "text",
              "errorMsg": "Please enter valid name"
            },
            {
              "name": "description",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Description",
              "inputType": "text",
              "errorMsg": "Please enter valid data"
            },
            {
              "name": "pages",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Pages",
              "placeholder": "Select Pages",
              "inputType": "uiMultiSelect",
              "errorMsg": "Please enter valid data",
              "optionValue" : "_id",
              "optionLabel" : "name",
              "callback" : this.getPages
            }
          ];
    }

    async getPages(){
        return this.props.dispatch(EntityService.getAll("pages", {pageSize : 100}));
    }

    saveEntity (entity) { 
        if(this.state.isEdit){
            let data = {};
            this.formData.forEach((input) => {
                let inputName = input["name"];
                data[inputName] = entity[inputName];
            }); 
            let response = this.props.dispatch(EntityService.update(this.state.entityName, data));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr("Role Updated Successfully");
                    this.goTo();
                }else{
                   this.setState({error: value.message});
                }
            }, reason => {
              console.error(reason); // Error!
            });
        }else{
            
            let data = {};
            this.formData.forEach((input) => {
                if(input.isEditModeDisplay === undefined || input.isEditModeDisplay !== true){
                    let inputName = input["name"];
                    data[inputName] = entity[inputName];
                }
            }); 
            let response = this.props.dispatch(EntityService.save(this.state.entityName, data));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr("Role Added Successfully");
                    this.goTo();
                }else{
                   this.setState({error: value.message});
                }
            }, reason => {
              console.error(reason); // Error!
            });
        }
    }
      goTo(){
        let { transition } = this.props;
        transition.router.stateService.go('roles');
      }
     
      componentWillMount(){
        this.getPages();
        const { entity } = this.props;
        if(entity && entity.name === this.state.pageName && entity.data !== undefined){
            const {data} = entity;
            if(data){
              this.formData.forEach((input) => {
                let inputName = input["name"];
                if(input.inputType === "uiMultiSelect"){
                  let uiMultiSelectValue = data[inputName];
                  uiMultiSelectValue.forEach((value) => {
                    value["value"] = value[input.optionValue];
                    value["label"] = value[input.optionLabel];
                  });
                  input["value"] = uiMultiSelectValue;
                }else{
                  input["value"] = data[inputName];
                }
                
              }); 
            }
            this.setState({ isEdit : true });
        }else{
          let object = {
            "name": this.state.pageName,
            "data": undefined
          }
          this.setState({ isEdit : false });
          this.props.dispatch(CommonService.setSelectedEntity(object));
        }
      }

      handleChange = selectedOption => {
        this.setState({ pages : selectedOption });
      }

render() {
      return (
        <div>
        <h3> 
            <span className="navLink" onClick={() => this.goTo()}>
             { this.state.isEdit === true ? 'Update' : 'New'} &nbsp;
              {this.state.pageName}  <Icon iconName="FaRegArrowAltCircleLeft"/> </span>
         </h3>
             { 
        this.state.error !== "" &&
        <div className="alert alert-danger ng-binding" role="alert">
            <strong>Error!</strong> {this.state.error}
        </div>
        }
        <div className="offset-md-2">
           <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12">
                <CustomForm formData={this.formData} onSave={this.saveEntity} isEdit = {this.state.isEdit} />
              </div>
           </div>
        </div>
     </div>
      )
    }
}

const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(RoleEntry);