import React, { Component } from "react";
import {connect} from "react-redux";
import * as CommonService from "../../../services/CommonService";
import * as EntityService from "../../../services/EntityService";
import { Icon } from '../../FontAwesome';
import { SuccessToastr } from '../../Toastr';
import  CustomForm from "../../Form/form.js";

class PageEntry extends Component{
    constructor(props){
        super(props);
        this.state = {
            isEdit : false,
            pageName : "Page",
            entityName : "pages",
            error : ""
        }
        this.saveEntity = this.saveEntity.bind(this)
        this.formData = [
            {
                "name": "_id",
                "value": "",
                "required": false,
                "regex": "",
                "label": "ID",
                "inputType": "text",
                "errorMsg": "",
                "isEditModeDisplay": true,
                "isDisabled": true
            },
            {
              "name": "name",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Name",
              "inputType": "text",
              "errorMsg": "Please enter valid name"
            },
            {
                "name": "url",
                "value": "",
                "required": true,
                "regex": "",
                "label": "URL",
                "inputType": "text",
                "errorMsg": "Please enter valid name"
            },
            {
              "name": "description",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Description",
              "inputType": "text",
              "errorMsg": "Please enter valid data"
            }
          ];
    }

    saveEntity (entity) { 
        if(this.state.isEdit){
            let data = {};
            this.formData.forEach((input) => {
                let inputName = input["name"];
                data[inputName] = entity[inputName];
            }); 
            let response = this.props.dispatch(EntityService.update(this.state.entityName, data));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr("Page Updated Successfully");
                    this.goTo();
                }else{
                   this.setState({error: value.message});
                }
            }, reason => {
              console.error(reason); // Error!
            });
        }else{
            let data = { name : entity.name,
                url : entity.url,
                description : entity.description
            };
            let response = this.props.dispatch(EntityService.save(this.state.entityName, data));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr("Page Added Successfully");
                    this.goTo();
                }else{
                   this.setState({error: value.message});
                }
            }, reason => {
              console.error(reason); // Error!
            });
        }
    }
      goTo(){
        let { transition } = this.props;
        transition.router.stateService.go('page');
      }
     
      componentWillMount(){
        const { entity } = this.props;
        if(entity && entity.name === this.state.pageName && entity.data !== undefined){
            const {data} = entity;
            if(data){
              this.formData.forEach((input) => {
                let inputName = input["name"];
                input["value"] = data[inputName];
              }); 
            }
            this.setState({ isEdit : true });
        }else{
          let object = {
            "name": this.state.pageName,
            "data": undefined
          }
          this.setState({ isEdit : false });
          this.props.dispatch(CommonService.setSelectedEntity(object));
        }
      }

render() {
      return (
        <div>
        <h3> 
            <span className="navLink" onClick={() => this.goTo()}>
             { this.state.isEdit === true ? 'Update' : 'New'} &nbsp;
              {this.state.pageName}  <Icon iconName="FaRegArrowAltCircleLeft"/> </span>
         </h3>
             { 
        this.state.error !== "" &&
        <div className="alert alert-danger ng-binding" role="alert">
            <strong>Error!</strong> {this.state.error}
        </div>
        }
        <div className="offset-md-2">
           <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12">
                <CustomForm formData={this.formData} onSave={this.saveEntity} isEdit = {this.state.isEdit} />
              </div>
           </div>
        </div>
     </div>
      )
    }
}

const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(PageEntry);