import React, {Component} from "react";
import {connect} from "react-redux";

import * as CommonService from "../../../services/CommonService";
import TextInput from '../../Form/textInput';
import { SuccessToastr } from '../../Toastr';
import { InputValidation } from '../../utility/Utility';

class Registration extends Component {

    constructor(props){
        super(props);
        
        this.formData = [
            {
              "name": "username",
              "value": "",
              "required": true,
              "regex": "",
              "label": "User Name",
              "inputType": "text",
              "errorMsg": "Please enter valid User Name",
              "minLength": 4,
              "maxLength": 12
            },
            {
              "name": "email",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Email",
              "inputType": "email",
              "errorMsg": "Please enter valid Email"
            },
            {
              "name": "password",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Password",
              "inputType": "password",
              "errorMsg": "Please enter valid Password"
            
            }
        ];
        let object = {};
        this.formData.forEach((input) => {
            let name = input["name"];
            object[name] =  input["value"];
        });
        this.state = object;
    }
    componentDidMount(){
        this.setState({error: ""});
    }
    onValueChange = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({ [name]: value });
    };
    goTo(pageName){
        this.props.transition.router.stateService.go(pageName);
    }

    handleClick (page){
      this.goTo(page);
    };
  

    register = (event) =>{
        let isValid = true;
        let inputName = "";
        let errorMessage = "";
       for (let i = 0; i < this.formData.length; i++) {
           let input = this.formData[i];
           isValid = InputValidation(input, this.state[input.name]);
           if(isValid !== true){
                inputName = input.name;
                errorMessage = isValid;
                isValid = false;
                break;
           }
        }
        if(this.state.password !== this.state.confirmPassword && isValid){
          isValid = false;  
          inputName = "confirmPassword";
          errorMessage = "Password  and Confirm Password do not match";
        }
        if(!this.state.accept){
          isValid = false;  
          inputName = "accept";
          errorMessage = "Please accept terms & Privacy Policy.";
        }
        if(isValid){
          this.setState({ errorInput : "", errorMsg : "" })
        }else{
            this.setState({ errorInput : inputName, errorMsg : errorMessage })
        }
        if(isValid){
            this.setState({ errorInput : "" })
            let dataObject = {};
            for (let i = 0; i < this.formData.length; i++) {
                let input = this.formData[i];
                dataObject[input.name] = this.state[input.name]
            }
            let response = this.props.dispatch(CommonService.registration(dataObject));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr("Registration Completed Successfully");
                    let { transition } = this.props;
                    transition.router.stateService.go('login');
                }else{
                   this.setState({error: value.message});
                }
              }, reason => {
                console.error(reason); // Error!
              });
         }else{
             this.setState({ errorInput : inputName })
         }
    }

    render() {
        const isError = false, errorMsg = "";
        return (
            <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div className="card-signin my-5">
                <div className="signup-form">
   <form className="border border-light">
            <h2>Register</h2>
            <p className="hint-text">Create your account. It's free and only takes a minute.</p>
      { 
        this.state.error !== "" &&
        <div className="alert alert-danger ng-binding" role="alert">
            <strong>Error!</strong> {this.state.error}
        </div>
        }

      { this.formData.map(input => (
                    <TextInput label={input.label} isError={ input.name === this.state.errorInput ? true : false } onValueChange={this.onValueChange} key={input.name} isEdit={false} isEditModeDisplay={input.isEditModeDisplay}
                    isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={this.state.errorMsg} />
        ))}
      <div className="form-group">
      <label className="required">Confirm Password </label>
          <input type="password" className="form-control" placeholder="Confirm Password" name="confirmPassword"
          onChange={this.onValueChange} required value={this.state.confirmPassword} />
          { "confirmPassword" === this.state.errorInput &&
                <div className="invalid-feedback">
                {this.state.errorMsg}
              </div>
                }
      </div>
      <div className="form-check">
			<label className="form-check-label">
                <input type="checkbox" required="required" className="form-check-input" name="accept" 
                onChange={this.onValueChange} checked={this.state.accept} />
                 &nbsp; I accept the <span className="navLink">Terms of Use</span> &amp; &nbsp;
                 <span className="navLink">Privacy Policy</span></label>
                 { "accept" === this.state.errorInput &&
                <div className="invalid-feedback">
                {this.state.errorMsg}
              </div>
                }
		</div>
    <br/>
      
      <button className="btn btn-success btn-lg btn-block"
       type="button" onClick={(e)=>{this.register()}}>Register Now</button>
        <br/>
    <div className="text-center">Already have an account? 
        <span className="navLink" onClick={() => this.handleClick('login')} name="login"> Sign in
        </span>
    </div>
   </form>
</div>
</div>
</div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
      loading: state.commonReducer.loading,
      isAuthenticated : state.commonReducer.isAuthenticated,
    }
  };

export default connect(mapStateToProps)(Registration);