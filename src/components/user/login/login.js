import React, {Component} from "react";
import {connect} from "react-redux";

import * as CommonService from "../../../services/CommonService";
import { Icon } from '../../FontAwesome';
import Footer from '../../footer/footer';

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
          username: "",
          password: "",
          isRemember: false,
          error : ""
        }
    }
    componentDidMount(){
      const { isAuthenticated, transition } = this.props;
      if(isAuthenticated ){
        transition.router.stateService.go("welcomepage");
      }
    }
    onValueChange = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({ [name]: value });
    };

    goTo(pageName){
      this.props.transition.router.stateService.go(pageName);
    }
    handleClick(page){
      this.goTo(page);
    };

    login(){
     let response = this.props.dispatch(CommonService.login( {username: this.state.username , password: this.state.password, remember: this.state.isRemember} ));
        
      response.then(value => {
          if(value.success === true){
            let data = {
              isAuthenticated: true,
              isAdmin: value.data.isSysRole,
              user : value.data
            }
            /**  store token in local storage ***/
            localStorage.setItem('AuthToken', ( "Bearer " + value.data.id_token));

            this.props.dispatch(CommonService.isUserAuthenticate(data));
              let { transition } = this.props;
              transition.router.stateService.go('welcomepage');
          }else{
            this.setState({error: value.message});
          }
      }, reason => {
          console.error(reason); // Error!
      });
    }
   
    render() {
        return (
            <div className="container">
            <div className="row">
              <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div className="card card-signin my-5">
                  <div className="card-body login-panel" id="triangle">
                    { 
                    this.state.error !== "" &&
                    <div className="alert alert-danger ng-binding" role="alert">
                        <strong>Error!</strong> {this.state.error}
                    </div>
                    }
                    <form className="form-signin">
                    <p className="text-primary-mute">Login with email / username </p>
                    <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <Icon iconName={"FaUser"} />
                      </span>
                  </div>
                  <input type="email" className="form-control" name="username"
                  value={this.state.username} onChange={this.onValueChange}
                   placeholder="Username or email" aria-label="Username" />
                </div>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <Icon iconName={"FaLock"} />
                      </span>
                  </div>
                  <input type="password" className="form-control" name="password"
                  value={this.state.password} onChange={this.onValueChange}
                   placeholder="Password" aria-label="Username" />
                </div>
                      <div className="custom-control custom-checkbox mb-3">
                        <input type="checkbox" className="custom-control-input" 
                        checked={this.state.isRemember} 
                        onChange={this.onValueChange} name="isRemember" />
                        <label className="custom-control-label text-primary-mute" htmlFor="remember password">Remember Me</label>
                      </div>
                      <button className="btn btn-md btn-primary btn-block loginButton" type="submit" 
                      onClick={() => this.login()}>Login <Icon iconName={"FaSignInAlt"} /> </button>
                    </form>
                    <div className="col-md-12" style={{marginTop: 0.5 + "em"}}>
                      <span className="navLink" onClick={() => this.handleClick('resetPassword')} name="resetPassword"> Reset Password</span>
                      <span className="navLink" style={{float: "right"}}
                      onClick={() => this.handleClick('register')} name="register">Sign Up</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer/>
          </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
      loading: state.commonReducer.loading,
      isAuthenticated : state.commonReducer.isAuthenticated,
    }
  };

export default connect(mapStateToProps)(Login);