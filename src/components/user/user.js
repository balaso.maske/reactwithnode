import React, {Component} from 'react';
import {connect} from "react-redux";
import { Icon } from '../FontAwesome';
import * as EntityService from "../../services/EntityService";
import * as CommonService from "../../services/CommonService";
import * as UserService from "../../services/UserService";
import Moment from 'react-moment';
import { FaEdit} from 'react-icons/fa';
import { SuccessToastr, ErrorToastr } from '../Toastr';
import Pagination from '@material-ui/lab/Pagination';
import TableHeader from '../Table/TableHeader';

class User extends Component {
    constructor(props){
        super(props);
        this.state = {
            "pageName": "User",
            "entityName": "users",
            pageSize : 2,
            page: 0,
            sortCol: "",
            sortReverse: true,
            searchText : "",
            data : { total : 0, data:[]}
        }

        this.onSortChange = this.onSortChange.bind(this);

        this.tableHeaders = [
          {
            "sortColumnName": "username",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "User Name"
          },
          {
            "sortColumnName": "email",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Email"
          },
          {
            "sortColumnName": "",
            "isDisplay": true,
            "isSortBySupport": false,
            "displayName": "Roles"
          },
          {
            "sortColumnName": "lastModifiedDate",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Updated"
          },
          {
            "sortColumnName": "activated",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Active"
          },
          {
            "sortColumnName": "",
            "isDisplay": true,
            "isSortBySupport": false,
            "displayName": "Action"
          }
        ];
    }
    onSortChange(sortColumn){
       this.setState({ sortCol : sortColumn, sortReverse : !this.state.sortReverse}, () => {
        this.fetchData();
       }); 
    }

    goTo(page){
        let { transition } = this.props;
        transition.router.stateService.go(page);
    }
  
  editData(user){
      let object = {
        "name": this.state.pageName,
        "data": user
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      let { transition } = this.props;
      transition.router.stateService.go("userProfile", { username: user.username});
  }

    deleteData(user){
      let response = this.props.dispatch(EntityService.remove(this.state.entityName, user.username));
      response.then(value => {
        if(value.success === true){
          SuccessToastr("User Deleted Successfully");
          this.fetchData();
        }else{
          ErrorToastr(value.message);
        }
      }, reason => {
        console.error(reason); // Error!
      });
    }

    componentDidMount() {
      let object = {
        "name": this.state.pageName,
        "data": undefined
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      this.fetchData();
    }
  
    fetchData(){
      let object = {
        page : this.state.page,
        pageSize : this.state.pageSize,
        sortCol : this.state.sortCol,
        orderBy: this.state.sortReverse ? "DESC" : "ASC",
        searchText : this.state.searchText
      }
      
      let responseNew = this.props.dispatch(EntityService.getAll(this.state.entityName, object));
      responseNew.then(value => {
          if(value.success === true){
              let data = value.data[0];
              this.setState({ data : { total: data.total[0].total, "data": data.data}});
          }else{
              ErrorToastr(value.message);
              this.setState({ data : { total: 0, "data": []}});
          }
      }, reason => {
        console.error(reason); // Error!
      });
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, page : 1 }, () => {
          this.fetchData();
        }); 
    };

    _handleSearchTextChange(e) {
      this.setState({ searchText : e.target.value });
   }

  pageHandleChange = (event, value) => {
      this.setState({ page : value }, () => {
          this.fetchData();
      }); 
  };

  handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.fetchData();
    }
  }

  userActivation(user, status){
    let responseNew;
    if(status === true){
      responseNew = this.props.dispatch(UserService.activateUser(user._id));
    }else{
      responseNew = this.props.dispatch(UserService.deactivateUser(user._id));
    }
  
    responseNew.then(value => {
        if(value.success === true){
         this.fetchData();
        }else{
            ErrorToastr(value.message);
        }
    }, reason => {
      console.error(reason); // Error!
    });
  }

  deactivateUser(user){
    let responseNew = this.props.dispatch(UserService.deactivateUser(user._id));
    responseNew.then(value => {
        if(value.success === true){
          this.fetchData();
        }else{
            ErrorToastr(value.message);
        }
    }, reason => {
      console.error(reason); // Error!
    });
    
  }
      
  render(){
        const { isAdmin, isAdminView } = this.props;
        const { data } = this.state.data;
    return(
        <div>
      <div className="well well-sm">
         <div className="row">
               <button type="button" className="col-sm-2 col-md-2 col-lg-2 btn btn-success"  
                  onClick={() => this.goTo('userEntry')} style={{marginLeft : 1 + 'em'}}>
                    New User  <Icon iconName="FaPlus"/>
               </button>
            <div className="col-xs-4 col-sm-3 col-md-3 col-lg-3 searchBTn">
          <div className="has-feedback">
              <input type="text" className="form-control borderradius" id="search" name="searchText"
                  placeholder="Search" onKeyDown={this.handleKeyDown} value={this.state.searchText}
                  onChange={event => { this.setState({ searchText: event.target.value})}} />
            </div>
		</div>
           
         </div>
      </div>
      <div>
      <div className="table-responsive">
         <table className="table table-striped table-hover">
            <thead>
               <tr>
                  { this.tableHeaders.map((header, index) => (
                      <TableHeader key={index} displayName={header.displayName} onSortChange={this.onSortChange} sortColumnName={header.sortColumnName} sortCol={this.state.sortCol} sortReverse={this.state.sortReverse} isSortBySupport={header.isSortBySupport} />
                  ))}
               </tr>
            </thead>
            <tbody>
            { data.map(entity => (
               <tr className="clickableElement" key={entity._id} onDoubleClick={() => this.editData(entity)}>
                  <td>{entity.username}</td>
                  <td>{entity.email}</td>
                  <td>
                  { entity.roles.map((data, index) => (
                      <span key={index} title="">{data.name} { index < ( entity.roles.length - 1) ? "," : ""} </span>
                  ))}
                  </td>
                  <td>
                    <span>{entity.lastModifiedBy} </span>
                      <div className="text-primary-mute">
                        <Moment fromNow>{entity.lastModifiedDate}</Moment>
                      </div> 
                    </td>
                 
                  
                  <td className="activateIcon">
                    { entity.activated && 
                      <span className="navLink" title="Click to disable User" onClick={() => this.userActivation(entity, false)}> 
                        <Icon iconName="FaToggleOn" size="24px" color="#337ab7"/>
                      </span>
                    }
                    {
                      entity.activated === false &&
                      <span className="navLink" title="Click to enable User" onClick={() => this.userActivation(entity, true)}>
                        <Icon iconName="FaToggleOff" size="24px" color="#337ab7"/>
                      </span>
                    }
                    </td>
                    <td className="chartFeatures" > 
                      <FaEdit onClick={() => this.editData(entity)} title="click to edit User"> Edit </FaEdit>
                  </td>
               </tr>
               ))
               }
            </tbody>
         </table>
      </div>
      <div>
      <div className="row">
                <div className="col-sm-10 col-md-10">
                  <div className="form-group ">
                      <label htmlFor="page">Page: {this.state.page}</label>
                      <Pagination variant="outlined" siblingCount={0}
                      size="large" color="primary"
                        count={Math.ceil(this.state.data.total / this.state.pageSize)} 
                        page={this.state.page} name="page" onChange={this.pageHandleChange} />
                  </div>
              </div>
              <div className="col-sm-2 col-md-2">
                    <label htmlFor="PageSize">Page Size</label>
                    <select className="form-control" value={this.state.pageSize} name="pageSize" onChange={this.handleChange}>
                                <option value="2">2</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                        </select>
              </div>
            </div>
      </div>
   </div>
   </div>
    )
    }
}

const mapStateToProps = state => ({
    isAdmin : state.commonReducer.isAdmin,
    isAdminView : state.commonReducer.isAdminView
});

export default connect(mapStateToProps)(User);