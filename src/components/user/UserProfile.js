import React, { Component } from "react";
import {connect} from "react-redux";
import * as UserService from "../../services/UserService";
import * as CommonService from "../../services/CommonService";
import "./userprofile.css";
import userImage from "../../../src/assets/image/userimage.png";
import Moment from 'react-moment';

class UserProfile extends Component{
    constructor(props){
        super(props);
        this.state = { user:{ roles :[]}, dataAvailable: true };
    }
    componentDidMount(){
        const { entity } = this.props;
        let username = this.props.$stateParams.username;
        let responseNew = this.props.dispatch(UserService.getUserByUsername(username));
        responseNew.then(value => {
            if(value.success === true){
             this.setState({user : value.data, dataAvailable: true});
            }else{
                this.setState({user : {roles :[]}, dataAvailable: false});
               // ErrorToastr(value.message);
            }
        }, reason => {
            console.error(reason); // Error!
        });
        if(entity && entity.name === "User" && entity.data !== undefined){
            const {data} = entity;
            if(data){
            }
        }
    }

    edit(){
        let object = {
            "name": "User",
            "data": this.state.user
          }
          this.props.dispatch(CommonService.setSelectedEntity(object));
          let { transition } = this.props;
          transition.router.stateService.go("userEntry");
    }

    goToRolePage(entity){
        let object = {
            "name": "Role",
            "data": entity
          }
          this.props.dispatch(CommonService.setSelectedEntity(object));
          let { transition } = this.props;
          transition.router.stateService.go("roleView", { name: entity.name});
    }

    renderUserProfile() {
        const {username, email, roles, createdBy, lastModifiedBy, createdDate, lastModifiedDate, passwordUpdatedDate, loginDate} = this.state.user;
        return (

<div id="content" className="content p-0">
    <div className="profile-header">
        <div className="profile-header-cover"></div>
        <div className="profile-header-content">
            <div className="profile-header-img">
                <img src={userImage} alt="" />
            </div>
            <div className="profile-header-info">
                <h4 className="m-t-sm">{username}</h4>
                <p className="m-b-sm">{email}</p>
                { 
                    (this.props.user.isSysRole || this.props.user.username === username ) && 
                    <span className="navLink"onClick={() => this.edit()} className="btn btn-xs btn-primary mb-3">Edit Profile</span>
                }
            </div>
        </div>
    </div>

    <div className="profile-container">
        <div className="row row-space-20">
            <div className="col-md-12 hidden-xs hidden-sm">
                <ul className="profile-info-list">
                    <li className="title">INFORMATION</li>
                    <li>
                        <div className="field">Roles:</div>
                        <div className="value">
                            { roles.map((data, index) => (
                                <> <span key={index} title="Click Here to View" className="navLink" onClick={() => this.goToRolePage(data)}> {data.name}  </span> { index < ( roles.length - 1) ? "," : ""}</>
                        ))}
                  </div>
                    </li>
                    <li>
                        <div className="field">Application:</div>
                        <div className="value">
                        { roles.map((data, index) => (
                             data.pages.map((page, i) =>(
                                <span key={i} title="">{page.name} { index < ( data.pages.length - 1) ? "," : ""} </span>
                            ))
                            
                        ))}
                        </div>
                    </li>
                </ul>
                <br/>
                <table className="table table-profile">
                            <thead>
                                <tr>
                                    <th colSpan="2">BASIC INFORMATION</th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                (this.props.user.isSysRole || this.props.user.username === username ) && 
                                    <tr>
                                        <td className="field">Last Login At:</td>
                                        <td className="value"><Moment format="MMMM Do YYYY, h:mm:ss a">{loginDate}</Moment></td>
                                        <td className="field">Last Password Updated At:</td>
                                        <td className="value">
                                            <Moment format="MMMM Do YYYY, h:mm:ss a">{passwordUpdatedDate}</Moment>
                                        </td>
                                    </tr>
                                }
                                
                                <tr>
                                    <td className="field">Last Modified By:</td>
                                    <td className="value">{lastModifiedBy}</td>
                                    <td className="field">Last Updated At:</td>
                                    <td className="value">
                                        <Moment format="MMMM Do YYYY, h:mm:ss a">{lastModifiedDate}</Moment>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="field">Created By:</td>
                                    <td className="value">{createdBy}</td>
                                    <td className="field">Created At:</td>
                                    <td className="value">
                                        <Moment format="MMMM Do YYYY, hh:mm:ss a">{createdDate}</Moment>
                                    </td>
                                </tr>
                       </tbody>
                       </table>

            </div>
        </div>
    </div>
</div>
        )
    }
    render(){
        return (
            <div className="container">

                    { this.state.dataAvailable && 
                        this.renderUserProfile()
                    }
                    { this.state.dataAvailable === false && 
                        <h2>User Profile Not available</h2>
                    }
            </div>
        )
    }
}


const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(UserProfile);