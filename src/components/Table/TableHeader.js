import React from "react";
import { Icon } from '../FontAwesome';

function TableHeader (props) {
    
        const { displayName, sortColumnName, sortCol, sortReverse, isSortBySupport} = props;

        if(isSortBySupport === false){
           return <th>  {displayName} </th>
        }else{
         return(  
              <th onClick={() => props.onSortChange(sortColumnName)} className="clickableElement">
                <span className={`clickableElement ${sortCol === sortColumnName ? "activeHeader" : ""}`}> {displayName} </span>
                <span className={sortCol === sortColumnName ? "activeHeaderIcon" : ""} >
                {
                    sortCol !== sortColumnName &&
                    <Icon iconName="FaSort"/>
                }
                {
                    ( sortCol === sortColumnName && !sortReverse) &&
                    <Icon iconName="FaCaretUp"/>
                }
                {
                    (sortCol === sortColumnName && sortReverse ) &&
                    <Icon iconName="FaCaretDown"/>
                }
                </span>
            </th>
        )
        }
    }

export default TableHeader;