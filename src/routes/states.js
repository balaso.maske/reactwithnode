
import App from '../App';
import Home from '../components/Home';
import WelcomePage from '../components/WelcomePage';
import TopMenubar from '../components/MenuBar/TopMenuBar';
import User from '../components/user/user';
import UserEntry from '../components/user/userEntry';
import ResetPasswordInit from '../components/user/ResetPassword/ResetPasswordInit';
import ResetPasswordFinish from '../components/user/ResetPassword/ResetPasswordFinish';
import Role from '../components/admin/roles/roles';
import Page from '../components/admin/Page/Page';
import Registration from '../components/user/Registration/Registration';
import Login from '../components/user/login/login';
import PageEntry from '../components/admin/Page/PageEntry';
import RoleEntry from '../components/admin/roles/RoleEntry';
import UserProfile from '../components/user/UserProfile';
import AboutMe from '../components/AboutMe';
import RoleView from '../components/admin/roles/RoleView';
import Dashboard from '../components/dashboard/dashboard';

const app = {
    name: 'app',
    redirectTo: 'home',
    component: App
  };

  const topMenubar = {
    parent: 'home',
    name: 'topMenuBar',
    url: '/home',
    component: TopMenubar
  };

  const user = {
    parent: 'home',
    name: 'user',
    url: '/user',
    component: User
  };

  
  const saveUser = {
    parent: 'home',
    name: 'userEntry',
    url: '/user/userEntry',
    component: UserEntry
  }

  const home = {
    parent: 'app',
    name: 'home',
    url: '/home',
    component: Home
  };

  const welcomepage = {
    parent: 'home',
    name: 'welcomepage',
    url: '/welcomepage',
    component: WelcomePage
  };
 
  const login = {
    parent: 'app',
    name: 'login',
    url: '/login',
    component: Login
  }
  
  const registration = {
    parent: 'app',
    name: 'register',
    url: '/register',
    component: Registration
  }
  
  const resetPassword = {
    parent: 'app',
    name: 'resetPassword',
    url: '/resetPassword',
    component: ResetPasswordInit
  };

  const resetPasswordFinish = {
    parent: 'app',
    name: 'resetPasswordFinish',
    url: '/resetPasswordFinish/:key',
    component: ResetPasswordFinish
  };

  const role = {
    parent: 'home',
    name: 'roles',
    url: '/roles',
    component: Role
  };

  const roleEntry = {
    parent: 'home',
    name: 'roleEntry',
    url: '/roleEntry',
    component: RoleEntry
  };

  const page = {
    parent: 'home',
    name: 'page',
    url: '/pages',
    component: Page
  };
  
  const pageEntry = {
    parent: 'home',
    name: 'pageEntry',
    url: '/pageEntry',
    component: PageEntry
  };

  const userProfile = {
    parent: 'home',
    name: 'userProfile',
    url: '/userProfile/:username',
    component: UserProfile
  };
  
  const roleView = {
    parent: 'home',
    name: 'roleView',
    url: '/roleView/:name',
    component: RoleView
  };
  

  const aboutMe = {
    parent: 'app',
    name: 'aboutMe',
    url: '/aboutMe',
    component: AboutMe
  };

  const dashboard = {
    parent: 'home',
    name: 'dashboard',
    url: '/dashboard',
    component: Dashboard
  };

  export default [app, login, home, welcomepage, topMenubar, user, saveUser, registration, resetPassword, resetPasswordFinish, role, page, pageEntry, roleEntry, userProfile, aboutMe, roleView, dashboard];