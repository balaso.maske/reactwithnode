import thunk from "redux-thunk";
import {createStore, combineReducers, applyMiddleware} from "redux";
import commonReducer from "./commonReducer";
import entityReducer from "./entityReducer";
import { SetInStorage, GetFromStorage } from '../../storage/storage';

const rootReducer = combineReducers({ 
  commonReducer,
  entityReducer
});

const persistedState = GetFromStorage();

const store = createStore(rootReducer, persistedState, applyMiddleware(thunk));

store.subscribe(()=>{
  SetInStorage(store.getState());
})

export default store;
