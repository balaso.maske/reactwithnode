export const API_CALL_BEGIN   = 'API_CALL_BEGIN';
export const API_CALL_END = 'API_CALL_END';
export const API_CALL_ERROR = 'API_CALL_ERROR';

export const SET_ENTITY = 'SET_ENTITY';

export const TOGGLE_LEFT_MENU = 'TOGGLE_LEFT_MENU';

export const IS_USER_AUTHENTICATE = 'IS_USER_AUTHENTICATE';

export const IS_ADMIN_VIEW = 'IS_ADMIN_VIEW';



