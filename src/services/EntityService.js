
import * as commonAction from "../../src/store/action/commonAction";
import axios from "axios";
const queryString = require('query-string');

const serverUrl = "http://localhost:4444/";


export function getAll(entityName, data) {
   // var start = (pageNo - 1) * pageSize;
    var params = {};
    let { page, pageSize, sortCol, orderBy, searchText } = data;

    if(sortCol === undefined || sortCol === ""){
        sortCol = "id";
    }
    if(orderBy === undefined || orderBy === ""){
        orderBy = "DESC";
    }

    if(page === undefined){
        page = 0;
    }else if(page !== 0){
      page = page - 1;
    }
    if(pageSize === undefined){
        pageSize = 2;
    }

    params["page"] = page;
    params["size"] = pageSize;
    params["sort"] = sortCol+","+orderBy;

    params = queryString.stringify(params);
    let url = serverUrl + "api/" + entityName;
    url = url + "?"+ params;
    
    return dispatch => {
        dispatch(commonAction.APICallBegin());
      return axios.get(url, requestHeaderWithAuth)
        .then(resp => {
            dispatch(commonAction.APICallEnd());
          return resp.data;
        })
        .catch(error =>{
            if(error.response){
              dispatch(commonAction.APICallEnd());
                return error.response.data;
              }
              dispatch(commonAction.APICallError(error))
            }
        );
    };
  };

export function remove(entityName, id){
    let url = serverUrl + "api/" + entityName + "/"+id;
    return callDeleteAPI(url, requestHeaderWithAuth);
}

export function callAPI(url, headers){
    return dispatch => {
        dispatch(commonAction.APICallBegin());
      return axios.get(url, headers)
        .then(resp => {
            dispatch(commonAction.APICallEnd());
          return resp.data;
        })
        .catch(error =>{
            if(error.response){
              dispatch(commonAction.APICallEnd());
                return error.response.data;
              }
              dispatch(commonAction.APICallError(error.message))
              return { success: false, data: [], message: error.message };
            }
        );
    };
}

export function callDeleteAPI(url, headers){
    return dispatch => {
        dispatch(commonAction.APICallBegin());
      return axios.delete(url, headers)
        .then(resp => {
            dispatch(commonAction.APICallEnd());
          return resp.data;
        })
        .catch(error =>{
              if(error.response){
                dispatch(commonAction.APICallEnd());
                return error.response.data;
              }
              dispatch(commonAction.APICallError(error.message))
              return { success: false, message: error.message };
            }
        );
    };
}

export function callPostAPI(url, requestBody, headers){
  return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.post(url, requestBody, headers, { crossDomain: true })
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      })
      .catch(error =>{
          if(error.response){
            dispatch(commonAction.APICallEnd());
            return error.response.data;
          }
          dispatch(commonAction.APICallError(error.message))
          return { success: false, message: error.message };
      });
  };
}

export function callPutAPI(url, requestBody, headers){
  return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.put(url, requestBody, headers, { crossDomain: true })
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      })
      .catch(error =>{
          if(error.response){
            dispatch(commonAction.APICallEnd());
            return error.response.data;
          }
          dispatch(commonAction.APICallError(error.message))
          return { success: false, message: error.message };
      });
  };
}

export function save(entityName, entity) {
  let url = serverUrl + "api/" + entityName;
  return callPostAPI(url, entity, requestHeaderWithAuth)
}

export function update(entityName, entity) {
  let url = serverUrl + "api/" + entityName;
  return callPutAPI(url, entity, requestHeaderWithAuth)
}

export const requestHeaderWithAuth = {
  headers: {
    'Content-Type':'application/json',
    "Access-Control-Allow-Origin": "*",
    "Authorization": localStorage.getItem("AuthToken")
  }
};

export const requestHeaderWithoutAuth = {
  headers: {
    'Content-Type':'application/json',
    "Access-Control-Allow-Origin": "*"
  }
};