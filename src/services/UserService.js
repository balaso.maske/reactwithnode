import * as EntityService from "./EntityService";

const serverUrl = "http://localhost:4444/";

export function activateUser(id) {
    let url = serverUrl + "api/users/activate/"+id;
    return EntityService.callAPI(url, EntityService.requestHeaderWithAuth);
};

export function deactivateUser(id) {
    let url = serverUrl + "api/users/deactivate/"+id;
    return EntityService.callAPI(url, EntityService.requestHeaderWithAuth);
};

export function getUserByUsername(username){
    let url = serverUrl + "api/users/" + username;
    return EntityService.callAPI(url, EntityService.requestHeaderWithAuth);
}