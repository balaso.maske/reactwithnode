import * as EntityService from "./EntityService";

const serverUrl = "http://localhost:4444/";

export function getRoleByName(name){
    let url = serverUrl + "api/roles/" + name;
    return EntityService.callAPI(url, EntityService.requestHeaderWithAuth);
}