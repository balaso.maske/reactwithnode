import React, { Component } from "react";
import './App.css';
import { UIView } from '@uirouter/react';
//import Footer from './components/footer/footer';
import {ToastContainer} from "react-toastify";
import Loader from "react-loader";
import {connect} from "react-redux";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }
  componentDidMount(){
   
  }
  componentWillReceiveProps(){
    
  }

  render() {
    const { isAuthenticated, transition } = this.props;
    if(isAuthenticated === false){
     //transition.router.stateService.go("login");
    }
  return (
    <Loader loaded={this.state.isLoading}
    lines={13}
    length={20}
    width={10}
    radius={30}
    corners={1}
    rotate={0}
    direction={1}
    color="#000"
    speed={1}
    trail={60}
    shadow={false}
    hwaccel={false}
    className="spinner"
    zIndex={2e9}
    top="50%"
    left="50%"
    scale={1.0}
    loadedClassName="loadedContent"
    >
       <div>
          <UIView/>
          <ToastContainer/>
        </div>
    </Loader>
  )
  };
}

const mapStateToProps = (state) => {
  return {
    loading: state.commonReducer.loading,
    isLeftMenuOpen : state.commonReducer.isLeftMenuOpen,
    isAuthenticated : state.commonReducer.isAuthenticated,
  }
};

export default connect(mapStateToProps, null)(App);
